package com.wwwgomes.algamoney.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wwwgomes.algamoney.api.model.Lancamento;
import com.wwwgomes.algamoney.api.repository.lancamento.LancamentoRepositoryQuery;

public interface LancamentoRepository extends JpaRepository<Lancamento, Long>, LancamentoRepositoryQuery {

}

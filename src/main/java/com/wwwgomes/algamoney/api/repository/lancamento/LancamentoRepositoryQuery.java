package com.wwwgomes.algamoney.api.repository.lancamento;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wwwgomes.algamoney.api.model.Lancamento;
import com.wwwgomes.algamoney.api.repository.filter.LancamentoFilter;
import com.wwwgomes.algamoney.api.repository.projection.ResumoLancamento;

public interface LancamentoRepositoryQuery {

	Page<Lancamento> filtrar(LancamentoFilter lancamentoFilter, Pageable pageable);

	Page<ResumoLancamento> resumir(LancamentoFilter lancamentoFilter, Pageable pageable);

}

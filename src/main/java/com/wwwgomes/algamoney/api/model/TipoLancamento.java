package com.wwwgomes.algamoney.api.model;

public enum TipoLancamento {

	RECEITA, DESPESA

}
